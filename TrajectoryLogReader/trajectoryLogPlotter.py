# Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)
# All rights reserved.
# 
# TrajectoryLogPlotter is an open source tool for TrueBeam Developer Mode (2.0 and higher) 
# provided by Varian Medical Systems, Palo Alto. 
# It lets users read the binary data provided trajectory log file after the beam delivery. 
# This version is based on the schema for TrueBeam 2.0 and higher. 
#
# TrajectoryLogPlotter is licensed under the Varian Open Source (VOS) License.
# You may obtain a copy of the License at:
#
#       website: http://radiotherapyresearchtools.com/license/
# 
# For questions, please send us an email at: TrueBeamDeveloper@varian.com                   
# 
# Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.
# 
# Created on: 12:47:06 PM, Jun 18, 2014
# Author: Pankaj Mishra and Thanos Etmektzoglou

import struct, sys
import numpy as np
from matplotlib import pyplot as plt
from argparse import ArgumentParser

class FileReader():
    '''
   The is the main trajectory file reader class.
    This class reads header, subbeam and axis data from the 
    binary trajectory log file.
    This class also lets the user plot various data stored in the 
    trajectory log file.
    '''
    
    def __init__(self, filename=None):
        '''
        Assign the filename and open the file 
        :param filename:
        '''        
        self.filename = filename
        self.openFile()
     
    def openFile(self):
        '''
        Check for the existence of the binary file
        and open it
        '''        
        try:
            #------------------- Message that a specific file has been generated
            self.f = open(self.filename, 'rb')    
            print "file " + self.filename + " exists" 
        except IOError:
            #------------------------ No trajectory file of the given name exits
            print "Binary trajectory file doesn't exist"        
           
    def headerData(self):
        '''
        Header has a fixed length of 1024 bytes. 
        Integers and floats are stored in little endian format 
        '''
        self.trajectoryHeader = dict()
        
        #----------------------------------------- Start reading the header info
        self.trajectoryHeader['Signature'] = self.f.read(16)
        self.trajectoryHeader['Version'] = self.f.read(16) 
        self.trajectoryHeader['HeaderSize'] = struct.unpack('<i', self.f.read(4))[0]
        self.trajectoryHeader['SamplingInterval'] = struct.unpack('<i', self.f.read(4))[0]
        self.trajectoryHeader['NumberOfAxes'] = struct.unpack('<i', self.f.read(4))[0] 
                
        #------------- Substitute axis in integer with their corresponding names
        self.axeslabel = {0:'Coll Rtn', 1:'Gantry Rtn',
                     2:'Y1', 3: 'Y2', 4: 'X1', 5: 'X2',
                     6:'Couch Vrt', 7:'Couch Lng', 8:'Couch Lat', 9:'Couch Rtn', 10:'Couch Pit', 11:'Couch Rol',
                     40:'MU', 41:'Beam Hold', 42:'Control Point', 50:'MLC', 60:'TargetPosition', 61:'Tracking Target',
                     62:'TrackingBase', 63:'TrackingPhase', 64:'TrackingConformityIndex'
                     }
                
        for i in range(self.trajectoryHeader['NumberOfAxes']):
            currentAxis = struct.unpack('<i', self.f.read(4))[0]
            self.trajectoryHeader[self.axeslabel[currentAxis]] = currentAxis 
                
        #------------ Number of sample per axis, generally 1, except for mlc 122
        samplePerAxis = np.zeros(self.trajectoryHeader['NumberOfAxes'])
            
        for i in range(self.trajectoryHeader['NumberOfAxes']):
            samplePerAxis[i] = struct.unpack('<i', self.f.read(4))[0] 
                    
        self.trajectoryHeader['SamplePerAxis'] = samplePerAxis
        self.trajectoryHeader['AxisScale'] = 'Machine Scale' if(struct.unpack('<i', self.f.read(4))[0] == 1) else 'Modified IEC'        
        self.trajectoryHeader['NumberOfSubbeams'] = struct.unpack('<i', self.f.read(4))[0]
        self.trajectoryHeader['Truncated'] ='truncated' if(struct.unpack('<i', self.f.read(4))[0] == 1) else 'Not truncated'
        self.trajectoryHeader['NumberOfSnapShots'] = struct.unpack('<i', self.f.read(4))[0]
        self.trajectoryHeader['MLCModel'] = 'NDS 120' if(struct.unpack('<i', self.f.read(4))[0] == 2) else 'NDS 120 HD'
        
        #------------------------ Rest of the header bytes are reserved for future use
        self.f.seek(1024)    
        
        
    def subbeamStrcuture(self):
        '''
        Read the subbeam data structure.
        Each subbeam is 560 bytes long 
        '''
        
        subbeam = dict()
        
        subbeam['cp'] = struct.unpack('<i', self.f.read(4))[0]
        subbeam['mu'] = struct.unpack('<f', self.f.read(4))[0]
        subbeam['radTime'] = struct.unpack('<f', self.f.read(4))[0]
        subbeam['Seq'] = struct.unpack('<i', self.f.read(4))[0]
        subbeam['NameOfTheSubbeam'] = self.f.read(512)
        subbeam['Reserved'] = self.f.read(32)
        
    def axisDataStructure(self):
        '''
        Read the axis data from the trajectory file.
        Data corresponding to each axis is stored in a row.
        The two matrices corresponding to the expected and actual
        axis data is stored.   
        '''
            
        numOfMlcLeaves = 122
        trackingAxesLength = 5
        
        #------------------------------------------ Determine the length of axes
        if(self.trajectoryHeader['NumberOfAxes'] == 16):
            axesLength = self.trajectoryHeader['NumberOfAxes'] - 1 + numOfMlcLeaves            
        elif(self.trajectoryHeader['NumberOfAxes'] == 21):
            axesLength = self.trajectoryHeader['NumberOfAxes'] - 1 + numOfMlcLeaves + trackingAxesLength
            
        #----- Initialize matrices corresponding to expected and actual axis data
        self.expectedAxisDataPerAxis = np.zeros((self.trajectoryHeader['NumberOfSnapShots'], axesLength), dtype='float')
        self.actualAxisDataPerAxis = np.zeros((self.trajectoryHeader['NumberOfSnapShots'], axesLength), dtype='float')
                                  
        #--------------------------------------- Read expected and actual values
        for i in  range(self.trajectoryHeader['NumberOfSnapShots']):
            for j in range(axesLength):
                self.expectedAxisDataPerAxis[i, j] = struct.unpack('<f', self.f.read(4))[0]
                self.actualAxisDataPerAxis[i, j] = struct.unpack('<f', self.f.read(4))[0]   
    
    def inputMlcLeaf(self, leafNumStr):
        if leafNumStr == 'MLC':
            while True:
                try:
                    mlcLeafNum = int(raw_input('Please enter a valid leaf number(1-60): '))
                    if (mlcLeafNum >= 1) and (mlcLeafNum <= 60):
                        break
                    else:
                        print('The range of mlc leaves is 1-60')
                except ValueError:
                    print('Not a valid integer')

        return mlcLeafNum       
            
    def plotAxesData(self, xaxis, yaxis):
        '''
        plot the actual axis data and difference between actual and 
        expected values. Left y-axis shows actual axes values 
        and right y-axis shows the deviation  
        '''

        axesOrder = {'CollRtn': 0, 'GantryRtn':1, 'Y1':2,  'Y2':3, 'X1':4,
                      'X2':5,'CouchVrt':6, 'Couch Lng':7, 'CouchLat':8, 
                     'CouchRtn':9, 'CouchPit':10, 'CouchRol':11,
                     'MU':12, 'BeamHold':13, 'ControlPoint':14, 
                     'MLC':15, 'TargetPosition':16, 'TrackingTarget':17,
                     'TrackingBase':18, 'TrackingPhase':19, 'TrackingConformityIndex':20
                     }
        
        # Initial range check
        if ((xaxis not in axesOrder.keys())  or (yaxis not in axesOrder.keys())):
            print 'Not a valid axis string.'
            print "type 'python FILENAME -h' for help"
            sys.exit(0)
        elif ((axesOrder[xaxis] > self.trajectoryHeader['NumberOfAxes']) or (axesOrder[xaxis] > self.trajectoryHeader['NumberOfAxes'])):
            print 'One of the trajectory axes you have selected is not present in the log file.'
            print 'There are only ' + str(self.trajectoryHeader['NumberOfAxes']) + ' axes.'
            sys.exit(0)  
                                
        # If 'MLC' ask for the leaf number
        # Loop till a valid entry  
        xaxisInd = None
        yaxisInd = None
         
        if xaxis == 'MLC':
            xaxisInd = 17 + self.inputMlcLeaf('MLC')
        elif yaxis == 'MLC':
            yaxisInd = 17 + self.inputMlcLeaf('MLC')
            
        # Now assign the index to empty index axis  
        if xaxisInd is None:
            xaxisInd = axesOrder[xaxis]
        
        if yaxisInd is None:
            yaxisInd = axesOrder[yaxis]
            
        # Start plotting
        fig, ax1 = plt.subplots()
        ax1.plot(self.actualAxisDataPerAxis[:,xaxisInd], self.actualAxisDataPerAxis[:, yaxisInd],  'b-')
        ax1.set_xlabel(xaxis)
        
        # Make the y-axis label and tick labels match the line color.
        ax1.set_ylabel(yaxis, color='b')
        for tl in ax1.get_yticklabels():
            tl.set_color('b')
            
        # Trick to get different scale on right (deviation Y Axis)
        ax2 = ax1.twinx()
        deviation = self.expectedAxisDataPerAxis[:, yaxisInd] - self.actualAxisDataPerAxis[:, yaxisInd]
        ax2.fill_between(self.actualAxisDataPerAxis[:, xaxisInd], deviation, color='green', alpha=.3)
        
        ax2.set_ylabel('Deviation', color='g')
        for tl in ax2.get_yticklabels():
            tl.set_color('g')

        # Set the x-limit of the lot        
        ax2.set_xlim(min(self.actualAxisDataPerAxis[:, xaxisInd]), max(self.actualAxisDataPerAxis[:, xaxisInd]))
        
        # Set the y-limit of the plot
        ax2.set_ylim(4*min(deviation), 4*max(deviation))

        # Now show the final plot
        plt.show()
        
def process_arguments(args):
    
    # Construct the parser
    parser = ArgumentParser(description='TrueBeam(TM) 2.0 and higher trajectory  file reader and plotter')

    # Add expected arguments
    # Name of the trajectory log file
    parser.add_argument('-f',
                        '--filename',
                        dest = 'fname',
                        type=str,
                        required=True,
                        help="Enter name of a binary trajectory log file"
                        )        
        
    # Add x-axis name         
    parser.add_argument('-x',
                        '--xaxis',
                        dest = 'trajectoryXAxis',
                        type=str,
                        default = 'MU',
                        help="Enter a trajectory axis (without single quotes) from following list: \
                        'CollRtn', 'GantryRtn', 'Y1', 'Y2', 'X1', 'X2', 'CouchVrt', 'CouchLng', 'CouchLat' \
                        'CouchRtn', 'CouchPit', 'CouchRol', 'MU', 'BeamHold', 'ControlPoint', 'MLC'"                        
                        )        

    # Add y-axis name         
    parser.add_argument('-y',
                        '--yaxis',
                        dest = 'trajectoryYAxis',
                        type=str,
                        default = 'GantryRtn',
                        help="Enter a trajectory axis (without single quotes) from the following list: \
                        'CollRtn', 'GantryRtn', 'Y1', 'Y2', 'X1', 'X2', 'CouchVrt', 'CouchLng', 'CouchLat' \
                        'CouchRtn', 'CouchPit', 'CouchRol', 'MU', 'BeamHold', 'ControlPoint', 'MLC'"                        
                        )      
                        
    # Version number
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')


    # Apply the parser to the argument list
    options = parser.parse_args(args)

    return vars(options)
            
def main():
       
    # Read the command line argument
    options = process_arguments(sys.argv[1:])
    # Create file object    
    x = FileReader(options['fname'])
    # Read-in header data    
    x.headerData()
    # Subbeam data structure    
    x.subbeamStrcuture()
    # Expected and actual data for various axes
    # corresponding to each snapshots   
    x.axisDataStructure()
    # Determine the index of axis        
    x.plotAxesData(options['trajectoryXAxis'], options['trajectoryYAxis'])
                    
if __name__ == "__main__":
    # Start reading the file
    main()
    